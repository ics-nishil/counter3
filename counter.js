let c1 = 0;
let c2 = 0;
let c3 = 0;

function start_c1(){
    sc1=setInterval(()=>{c1++;document.getElementById('counter1').innerHTML=c1;},100)
}


function start_c2(){
    sc2=setInterval(()=>{c2++;document.getElementById('counter2').innerHTML=c2;},100)
}


function start_c3(){
    sc3=setInterval(()=>{c3++;document.getElementById('counter3').innerHTML=c3;},100)
}


function start(){
    sc1=setInterval(()=>{c1++;document.getElementById('counter1').innerHTML=c1;},100)
    sc2=setInterval(()=>{c2++;document.getElementById('counter2').innerHTML=c2;},100)
    sc3=setInterval(()=>{c3++;document.getElementById('counter3').innerHTML=c3;},100)
}


function stop_c1(){
    clearInterval(sc1)
    document.getElementById('counter1').innerHTML=c1
}


function stop_c2(){
    clearInterval(sc2)
    document.getElementById('counter2').innerHTML=c2
}


function stop_c3(){
    clearInterval(sc3)
    document.getElementById('counter3').innerHTML=c3
}



function stop(){
    clearInterval(sc1)
    document.getElementById('counter1').innerHTML=c1
    clearInterval(sc2)
    document.getElementById('counter2').innerHTML=c2
    clearInterval(sc3)
    document.getElementById('counter3').innerHTML=c3
}


function resume_c1(){
    if(c1!=0){
        sc1=setInterval(()=>{c1++;document.getElementById('counter1').innerHTML=c1;},100)        
    }
}

function resume_c2(){
    if(c2!=0){
        sc2=setInterval(()=>{c2++;document.getElementById('counter2').innerHTML=c2;},100)
    }
}


function resume_c3(){
    if(c3!=0){
        sc3=setInterval(()=>{c3++;document.getElementById('counter3').innerHTML=c3;},100)
    }
}


function resume(){
    if(c1!=0){
        sc1=setInterval(()=>{c1++;document.getElementById('counter1').innerHTML=c1;},100)        
    }
    if(c2!=0){
        sc2=setInterval(()=>{c2++;document.getElementById('counter2').innerHTML=c2;},100)
    }
    if(c3!=0){
        sc3=setInterval(()=>{c3++;document.getElementById('counter3').innerHTML=c3;},100)
    }
}

function reset_c1(){
    clearInterval(sc1)
    c1=0
    document.getElementById('counter1').innerHTML=c1
}


function reset_c2(){
    clearInterval(sc2)
    c2=0
    document.getElementById('counter2').innerHTML=c2
}


function reset_c3(){
    clearInterval(sc3)
    c3=0
    document.getElementById('counter3').innerHTML=c3
}



function reset(){
    clearInterval(sc1)
    c1=0
    document.getElementById('counter1').innerHTML=c1
    
    clearInterval(sc2)
    c2=0
    document.getElementById('counter2').innerHTML=c2
    
    clearInterval(sc3)
    c3=0
    document.getElementById('counter3').innerHTML=c3
}
